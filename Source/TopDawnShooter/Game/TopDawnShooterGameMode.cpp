// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDawnShooterGameMode.h"
#include "TopDawnShooterPlayerController.h"
#include "TopDawnShooter/Character/TopDawnShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopDawnShooterGameMode::ATopDawnShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDawnShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}