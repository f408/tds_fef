// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TopDawnShooterGameMode.generated.h"

UCLASS(minimalapi)
class ATopDawnShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATopDawnShooterGameMode();
};



