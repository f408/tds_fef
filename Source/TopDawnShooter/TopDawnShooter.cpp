// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDawnShooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TopDawnShooter, "TopDawnShooter" );

DEFINE_LOG_CATEGORY(LogTopDawnShooter)
 