// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h" 
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8 
{
	Aim_State UMETA(DisplayNAme = "Aim State"),
	AimWalk_State UMETA(DisplayNAme = "AimWalk State"),
	Walk_State UMETA(DisplayNAme = "Walk State"),
	Run_State UMETA(DisplayNAme = "Run State"),
	SprintRun_State UMETA(DisplayNAme = "SprintRun State")


};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintRunSpeedRun = 800.0f;

};

UCLASS()
class TOPDAWNSHOOTER_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
